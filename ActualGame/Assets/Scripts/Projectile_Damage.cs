﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile_Damage : MonoBehaviour
{
    public int projectileDamage;
    public float time;
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        Destroy(gameObject, time);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        collision.GetComponent<OgreController>().TakeDmg(projectileDamage);
        Debug.Log(collision + " took " + projectileDamage + " Remaining HP: " + collision.GetComponent<OgreController>().ogreHealth);
        Destroy(gameObject);
    }

}