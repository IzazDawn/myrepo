﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour
{

    public Slider PlayerHealthBar;
    public Slider PlayerStaminabar;
    

    Rigidbody2D rb;

    public Joystick joystick;

    public Animator animator;

    
    public GameObject Defeat;
    public GameObject canvas;
    public GameObject canvas2;
    


    /************ Health and Stamina ***********/
    public int playerHealth;
    public int playerStamina;
    public int staminaRegen;

    public void RemoveStamina(int staminaCost)
    {
        playerStamina -= staminaCost;
        PlayerStaminabar.value = playerStamina;

    }

    public void RemoveHealth(int TakeDmg)
    {
        playerHealth -= (TakeDmg-playerDefense);
        PlayerHealthBar.value = playerHealth;
        if (playerHealth <= 0)
        {
            
            canvas.gameObject.SetActive(false);
            canvas2.gameObject.SetActive(true);
            Defeat.SetActive(true);
        }
    }
    public void RetryButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    float staminaRegenTick;
    private void RegenStamina()
    {
       
        staminaRegenTick += Time.deltaTime;
        if (staminaRegenTick >= 1f && playerStamina+staminaRegen < 100)
        {
            playerStamina += staminaRegen;
            PlayerStaminabar.value = playerStamina;
            staminaRegenTick = 0;
        } else if (staminaRegenTick >= 1f && playerStamina + staminaRegen > 100)
        {
            playerStamina = 100;
            PlayerStaminabar.value = playerStamina;
            staminaRegenTick = 0;
        }
    }
    /*************    Movement    *************/

    public float playerSpeed = 10.0f;
    public bool facingRight = true;
    float moveX;
    private bool canMove = true;
    public LayerMask whatIsEnemies;
    


   


    

    public void PlayerMove()
    {

        if (joystick.Horizontal > 0)
        {
            moveX = 1;
        }
        else if (joystick.Horizontal < 0)
        {
            moveX = -1;
        }
        else
        {
            moveX = 0;
        }

        if (moveX != 0 && canMove == true)
        {
            rb.velocity = new Vector2(moveX * playerSpeed, rb.velocity.y);
            animator.SetBool("IsWalking", true);
        }
        else
        {
            animator.SetBool("IsWalking", false);
        }

        FlipPlayer();

    }

    void FlipPlayer()
    {
        if (moveX > 0.0f && facingRight != true || moveX < 0.0f && facingRight == true)
        {
            facingRight = !facingRight;
            Vector2 localscale = gameObject.transform.localScale;
            localscale.x *= -1;
            gameObject.transform.localScale = localscale;
        }

    }



    /*************    Jump    *************/
    public float jumpPower = 150.0f;
    public LayerMask groundLayer;

    public bool IsGrounded()
    {
        
        Vector3 leftRayPosition = gameObject.GetComponent<Collider2D>().bounds.center;
        Vector3 rightRayPosition = gameObject.GetComponent<Collider2D>().bounds.center;
        leftRayPosition.x -= gameObject.GetComponent<Collider2D>().bounds.extents.x;
        rightRayPosition.x += gameObject.GetComponent<Collider2D>().bounds.extents.x;

        if (Physics2D.Raycast(leftRayPosition, Vector2.down, 0.2f, groundLayer) || Physics2D.Raycast(rightRayPosition, Vector2.down, 0.2f, groundLayer))
        {
            return true;
        }
        else
        {
            return false;
        }


    }

    public void Jump()
    {
        RemoveHealth(100);
        if (IsGrounded())
        {
            Debug.Log("JumpActivated");
            rb.AddForce(new Vector2(0, jumpPower));
        }
    }




    //***********************************************************************************************Skills****************************************************************************************//

    private int currentWeapon;

    //Switch Stance

    public void SwitchStance()
    {
        Debug.Log("StanceSwitch");
        if (currentWeapon == 0)
        {
            currentWeapon = 1;
        }
        else
        {
            currentWeapon = 0;
        }

        animator.SetInteger("PlayerStance", currentWeapon);
        playerStamina = 50;
        Debug.Log("StanceSwitch");

        if (playerStamina > 50)
        {
            playerStamina = 50;
        }

    }

    //******* Bow Skills ******//
    public GameObject firePoint;
    private Vector3 firePointPosition;
    private Quaternion firePointRotation;
    public Rigidbody2D lightArrow;
    public Rigidbody2D semiChargedArrow;
    public Rigidbody2D chargedArrow;
    public float lightArrowSpeed;
    private bool isCharging = false;



    // Bow Light


    public void BowLight()
    {
        
        if (playerStamina >= 5)
        {
            RemoveStamina(5);
            Debug.Log(playerStamina);

            firePointPosition = firePoint.transform.position;
            firePointRotation = firePoint.transform.rotation;
            if (facingRight)
            {
                firePointRotation.z = 0f;
            }
            else
            {
                firePointRotation.z = 180f;
            }
            var firedLightArrow = Instantiate(lightArrow, firePointPosition, firePointRotation);
            firedLightArrow.AddForce(firePoint.transform.up * lightArrowSpeed);
            animator.SetTrigger("BowAttack");
            Debug.Log("BowLight");

        } else if (playerStamina < 5)

        {

            Debug.Log("NoStamina");

        }
    }
       
    


    // Bow Heavy
    private float currentCharge = 0f;
    private float neededCharge = 1.5f;

    public void BowHeavyStartCharge() {

        if (playerStamina >= 20)
        {
            isCharging = true;
            RemoveStamina(20);
        }
        else
        {
            Debug.Log("NoStamina");
        }
    }

    public void BowHeavyCharge()
    {
        if (isCharging)
        {
            canMove = false;
            currentCharge += Time.deltaTime;
            if (currentCharge < neededCharge)
            {
                animator.SetBool("IsCharging", true);
            } else
            {
                animator.SetBool("IsCharging", false);
                animator.SetBool("IsCharged", true);
            } 
            Debug.Log("charging" + currentCharge);

        }
    }


    public void BowHeavyRelease()
    {
        canMove = true;
        animator.SetBool("IsCharging", false);
        animator.SetBool("IsCharged", false);
        if (isCharging && currentCharge >= neededCharge)
        {
            //fire a charged shot
            firePointPosition = firePoint.transform.position;
            firePointRotation = firePoint.transform.rotation;
            if (facingRight)
            {
                firePointRotation.z = 0f;
            }
            else
            {
                firePointRotation.z = 180f;
            }
            var firedLightArrow = Instantiate(chargedArrow, firePointPosition, firePointRotation);
            firedLightArrow.AddForce(firePoint.transform.up * lightArrowSpeed);
            currentCharge = 0;
            animator.SetBool("IsCharging", false);
            Debug.Log("charged shot");

        }
        else if (isCharging && currentCharge >= neededCharge / 2)
        {
            //fire a semi charged shot
            firePointPosition = firePoint.transform.position;
            firePointRotation = firePoint.transform.rotation;
            if (facingRight)
            {
                firePointRotation.z = 0f;
            }
            else
            {
                firePointRotation.z = 180f;
            }
            var firedLightArrow = Instantiate(semiChargedArrow, firePointPosition, firePointRotation);
            firedLightArrow.AddForce(firePoint.transform.up * lightArrowSpeed);
            currentCharge = 0;
            Debug.Log("semi-charged shot");


        }
        else if (isCharging)
        {
            
            //fire a normal shot
            firePointPosition = firePoint.transform.position;
            firePointRotation = firePoint.transform.rotation;
            if (facingRight)
            {
                firePointRotation.z = 0f;
            }
            else
            {
                firePointRotation.z = 180f;
            }
            var firedLightArrow = Instantiate(lightArrow, firePointPosition, firePointRotation);
            firedLightArrow.AddForce(firePoint.transform.up * lightArrowSpeed);
            currentCharge = 0;
            Debug.Log("non-charged shot");

        }
        isCharging = false;



    }


    // Bow Special

    float flippingTime;
    bool isFlipping;

    public void BowSpecial()
    {

        if (IsGrounded() && playerStamina >= 25)
        {
            canMove = false;
            isFlipping = true;
            flippingTime = Time.time;
            animator.SetTrigger("BowFlip");
            RemoveStamina(25);
            if (facingRight)
            {
                rb.velocity = new Vector2(0, 0);
                rb.AddForce(new Vector2(-100, 150));

            }
            else
            {
                rb.velocity = new Vector2(0, 0);
                rb.AddForce(new Vector2(100, 150));
            }
            
        } else if (IsGrounded() && playerStamina < 25)
        {
            Debug.Log("NoStamina");
        }

        
    }

    void FlipTimer()
    {
        if (isFlipping && Time.time - flippingTime > 0.4f)
        {
            isFlipping = false;
            canMove = true;
        }
    }

    //****** Sword Skill ******//

    public GameObject melee1Point;
    public float melee1RangeX;
    public float melee1RangeY;
    public int melee1Damage;

    //Sword Light
    public void SwordLight()
    {

        {
            Debug.Log("SwordLight");
            animator.SetTrigger("SwordLight");
            Collider2D[] enemiesToDamage = Physics2D.OverlapBoxAll(melee1Point.transform.position, new Vector2(melee1RangeX, melee1RangeY), 0, whatIsEnemies);
            playerStamina -= 20;

            for (int i = 0; i < enemiesToDamage.Length; i++)
            {
                enemiesToDamage[i].GetComponent<OgreController>().TakeDmg(melee1Damage);
                Debug.Log(enemiesToDamage[i].name + " took " + melee1Damage + " Remaining HP: " + enemiesToDamage[i].GetComponent<OgreController>().ogreHealth);
            }
        }
    }

    //Sword Heavy

    public GameObject melee2Point;
    public float melee2Range;
    public int melee2Damage;
    public float melee2ForceX;
    public float melee2ForceY;

    public void SwordHeavy()
    {
        if (playerStamina >= 20)
        {
            RemoveStamina(20);
            rb.velocity = new Vector2(0, 0);
            if (facingRight)
            {
                rb.AddForce(new Vector2(melee2ForceX, melee2ForceY));
                Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(melee2Point.transform.position, melee2Range, whatIsEnemies);
                for (int i = 0; i < enemiesToDamage.Length; i++)
                {
                    enemiesToDamage[i].GetComponent<OgreController>().TakeDmg(melee2Damage); 
                    Debug.Log(enemiesToDamage[i].name + " took " + melee2Damage + " Remaining HP: " + enemiesToDamage[i].GetComponent<OgreController>().ogreHealth);
                }
            }
            else
            {
                rb.AddForce(new Vector2(-melee2ForceX, melee2ForceY));
                Collider2D[] enemiesToDamage = Physics2D.OverlapCircleAll(melee2Point.transform.position, melee2Range, whatIsEnemies);
                for (int i = 0; i < enemiesToDamage.Length; i++)
                {
                    enemiesToDamage[i].GetComponent<OgreController>().TakeDmg(melee2Damage);
                    Debug.Log(enemiesToDamage[i].name + " took " + melee2Damage + " Remaining HP: " + enemiesToDamage[i].GetComponent<OgreController>().ogreHealth);
                }
            }

            animator.SetTrigger("SwordHeavy");

        } else
        {
            Debug.Log("NoStamina");
        }
    }
    //Sword Special
    public int defenseBoost;
    int playerDefense;

    public void SwordSpecial()
    {
        if (playerStamina >= 25)
        {
            RemoveStamina(25);
            animator.SetBool("IsShielding", true);
            canMove = false;
            playerDefense = defenseBoost;
        } else
        {
            Debug.Log("NoStamina");
        }
    }

    public void SwordSpecialStop()
    {
        canMove = true;
        animator.SetBool("IsShielding", false);
        playerDefense = 0;
    }

    // Use this for initialization
    void Start()
    {
        rb = gameObject.GetComponent<Rigidbody2D>();
        animator = gameObject.GetComponent<Animator>();
        currentWeapon = 0;
        playerStamina = 100;
        playerHealth = 100;
        staminaRegenTick = 0;
        playerDefense = 0;

    }

    // Update is called once per frame
    void Update()
    {
        RegenStamina();
        PlayerMove();
        FlipPlayer();
        IsGrounded();
        BowHeavyCharge();
        FlipTimer();



    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(melee1Point.transform.position, new Vector3(melee1RangeX, melee1RangeY, 0));
    }

}