﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Health_Component : MonoBehaviour
{


    public int healthPoints;

    // Use this for initialization
    void Start()
    {
        healthPoints = 20;
    }

    // Update is called once per frame
    void Update()
    {
        if (healthPoints <= 0)
        {
            Debug.Log("Ogre was destroyed");
            Destroy(gameObject);

        }
    }
}
