﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class OgreController : MonoBehaviour
{
    
    public int ogreHealth;
    public float moveSpeed;
    private GameObject playerObject;
    private Animator animator;
    public Slider OgreHealthBar;
    public BoxCollider2D territory;
    public bool playerInTerritory;
    private float distance;
    public float minDistance;
    public float maxDistance;
    private bool isAttacking;
    private bool isAlive;
    public GameObject Victory;
    public GameObject canvas;
    public GameObject canvas2;

    /*************** Health ****************/

    public void TakeDmg (int dmg)
    {
        ogreHealth -= dmg;
        OgreHealthBar.value = ogreHealth;
        if (ogreHealth <= 0)
        {
            isAlive = false;
            animator.SetTrigger("Death");
            
            canvas.gameObject.SetActive(false);
            canvas2.gameObject.SetActive(true);
            Victory.SetActive(true);
        }
    }
    public void RetryButton()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    // Start is called before the first frame update
    void Start()
    {
        isAlive = true;
        playerInTerritory = false;
        animator = gameObject.GetComponent<Animator>();
        playerObject = GameObject.FindGameObjectWithTag("Player");
    }

    

    // Update is called once per frame
    void Update()
    {
        PlayerInTerritory();
        if (isAttacking)
        {
            switch (Random.Range(0, 3))
            {
                case 0:
                    if (!hasAttacked) {
                        OgreSwipe();
                    }
                    break;
                case 1:
                    if (!hasAttacked)
                    {
                        OgreSmash();
                    }
                    break;
                case 2:
                    if (!hasAttacked)
                    {
                        OgreKick();
                    }
                    break;
            }
            UpdateAttackTimer();
        }
        if (!isAttacking) {
                MoveToPlayer();
            }
        UpdateDistance();
    }

  
    bool PlayerInTerritory()
    {
        if (territory.IsTouching(playerObject.GetComponent<Collider2D>()))
        {
            return true;
        } else
        {
            return false;
        }
        
    }
    
    void MoveToPlayer()
    {
        if (PlayerInTerritory() && distance > minDistance && distance < maxDistance)
        {
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(0, 0);
            animator.SetBool("IsWalking", false);
            isAttacking = true;

        } else if (PlayerInTerritory() && distance > maxDistance)
        {
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Sign(playerObject.transform.position.x - gameObject.transform.position.x) * moveSpeed, 0);
            animator.SetBool("IsWalking", true);

        } else if (PlayerInTerritory() && distance < minDistance)
        {
            gameObject.GetComponent<Rigidbody2D>().velocity = new Vector2(Mathf.Sign(-playerObject.transform.position.x - gameObject.transform.position.x) * moveSpeed, 0);
            animator.SetBool("IsWalking", true);
        }
    }

    void UpdateDistance()
    {
        distance = Mathf.Abs(playerObject.transform.position.x - gameObject.transform.position.x);
        //Debug.Log(distance);
    }

    //attacks
    private bool hasAttacked;
    public float attackDelay;
    private float attackTimer;

    void UpdateAttackTimer()
    {
        if (attackTimer > 0)
        {
            attackTimer -= Time.deltaTime;
        } else
        {
            isAttacking = false;
            hasAttacked = false;
        }
    } 

    //Ogre Swipe
    void OgreSwipe()
    {
        animator.SetTrigger("Swipe");
        Debug.Log("Swipe");
        attackTimer = attackDelay;
        hasAttacked = true;
    }

    //Ogre Smash
    void OgreSmash()
    {
        animator.SetTrigger("Smash");
        Debug.Log("Smash");
        attackTimer = attackDelay;
        hasAttacked = true;
    }
    // Ogre Kick
    void OgreKick()
    {
        animator.SetTrigger("Kick");
        Debug.Log("Kick");
        attackTimer = attackDelay;
        hasAttacked = true;
    }

}
